<?php
if(!isset($_SESSION)) session_start();

define('BASE_PATH', realpath(__DIR__.'/../'));

/**
* Autoload
*/
$loader = require_once BASE_PATH . '/vendor/autoload.php';


//$loader->addPsr4('App\\', BASE_PATH . "/app");



//require (BASE_PATH."/app/classes/Env.php");

/**
* php DotENV
*/
new \App\Classes\Env(BASE_PATH);

/**
* Routing
*/
$router = new AltoRouter();
$router->setBasePath(getenv('APP_PATH'));
//$router->map('GET','/home','target','name');

require_once BASE_PATH."/app/routing/api.php";


//print_r($router->match());

//Array ( [target] => target [params]=>Array ( ) [name]=>name)

new \App\Classes\RoutingDispatcher($router);
