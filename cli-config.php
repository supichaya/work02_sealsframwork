<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

//require_once 'bootstrap.php';
//ยกไฟล์มาจาก bootstrap เดิม
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$isDevMode = false;

$dbParam = array ('url'=> "sqlsrv://seals:seals@localhost\SQLEXPRESS/seals?charset=utf-8");
// $dbParam = array (
//     'url'=> 'oci8://seals:seals@localhost/orcl?charset=utf8'
// );

$paths = array(__DIR__ . "/app/Entities");

$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
//entityManager เป็น database แบบ logical
$entityManager = EntityManager::create($dbParam,$config);
// จบไฟล์ที่ยกมา

return ConsoleRunner::createHelperSet($entityManager);

//$dbParam = array ('url'=> "oci8://seals:seals@localhost/orcl?charset=utf8");

//$dbParam = array ('url'=> "mysql://root:@localhost/seals?charset=utf8");