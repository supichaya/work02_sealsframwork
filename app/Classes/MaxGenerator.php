<?php

namespace App\Classes;

use Doctrine\ORM\Id\AbstractIdGenerator;

class MaxGenerator extends AbstractIdGenerator
{
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        //do somethine
        
        //get_class ([ object $object ] ) : string;

        // $query = $entityManager->createQuery('SELECT max(p.positionId) FROM Position p');
        // $query->setParameter('id',3);

        //ใช้ 3 บรรทัดด้านล่าง
        // $entityName=get_class($entity);
        // $allRecord=$em->getRepository($entityName)->findAll();
        // return count($allRecord)+1;

        //return 200;

        $pkKey=$entity->getPK();
        var_dump ($pkKey);

        $entityName=get_class($entity);
        $query=$em->createQueryBuilder()
        ->select ('coalesce(max(a.'.$pkKey.')+1,1) as maxPk')
        ->from($entityName,'a')
        ->getQuery();

        $a=$query->getOneOrNullResult();
        $maxGen=$a['maxPk'];


        return $maxGen;


    }
}
