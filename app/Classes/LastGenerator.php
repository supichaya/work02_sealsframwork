<?php

namespace App\Classes;

use Doctrine\ORM\Id\AbstractIdGenerator;
use App\Entities\LastNumber;

class LastGenerator extends AbstractIdGenerator
{
    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        //do something
        
        //get_class ([ object $object ] ) : string;

        // $query = $entityManager->createQuery('SELECT max(p.positionId) FROM Position p');
        // $query->setParameter('id',3);
        
        $length=$entity->getLength();
        $prefix=$entity->getPrefix();

        $entityName=get_class($entity);
        //get ค่า last_no ล่าสุดจาก Table last_nember
        $allRecord=$em->getRepository(LastNumber::class)->findOneBy([
            //select ค่าล่าสุด
            'key'=>$entityName,
            'prefix'=>$prefix
            
        ]);
        $lastNumber=1;
        //Check ว่ามีข้อมูลใน Table Last_Number หรือไม่
            if($allRecord!=null){
                //var_dump($allRecord);
                // มีแล้วเพิ่มค่า และ update table last number
               $lastNumber=$allRecord->getLastNo();
               $lastNumber++;
               $allRecord->setLastNo($lastNumber);

              $em->flush();
            }
            else{
               // ยังไม่มีก็เพิ่ม record ใน table last number
              //  $allRecord->setLastNo($lastNumber);
            $allRecord=new LastNumber();
            $allRecord->setLastNo($lastNumber);
            $allRecord->setKey($entityName);
            $allRecord->setPrefix($prefix);
            $em->persist($allRecord);
            $em->flush();

            }
            
        //return count($allRecord)+1;

        //return $lastNumber;
        
        //  ได้เลขสุดท้าย และ check ว่าต้อง generate พร้อม prefix หรือไม่

        // ถ้า prefix>0(strlen คือ นับจำนวนตัวอักษรใน ตัวแปร $prefix)
            
        if(strlen($prefix)>0){
            //$str = "Hello World";
            $lastNumber=str_pad($lastNumber,$length,"0",STR_PAD_LEFT);
            return $prefix .$lastNumber;
        }else{
            return $lastNumber;
        }



    }
}
