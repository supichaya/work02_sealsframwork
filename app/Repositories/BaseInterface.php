<?php
namespace App\Repositories;
interface BaseInterface {
	public function list ();
	public function get ($id);
	public function save ($data);
	public function delete ($id);
}
