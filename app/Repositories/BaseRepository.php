<?php 
namespace App\Repositories;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
class BaseRepository { 
private $entity;
public function __construct() {
	$isDevMode = false;
	$dbParam = array (
		'url' => getenv("DB_URI")
	); 
	$paths = array(BASE_PATH . "/app/entities");
	$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
	$this->entity = EntityManager::create($dbParam,$config); 
	$dbh = $this->entity->getConnection();
	
	// $sth=$dbh->prepare("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
	// $sth->execute();
}
protected function getEntityManager() {
	return $this->entity;
}
}
